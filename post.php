<?php
session_start();

// redirecting to login if token not set
if (!isset($_SESSION["token"]))
  {
    header("Location:http://insignia14.in");
  }  

// including files
require_once( 'Facebook/FacebookSession.php');
require_once( 'Facebook/FacebookRequest.php' );
require_once( 'Facebook/FacebookResponse.php' );
require_once( 'Facebook/FacebookSDKException.php' );
require_once( 'Facebook/FacebookRequestException.php' );
require_once( 'Facebook/FacebookRedirectLoginHelper.php');
require_once( 'Facebook/FacebookAuthorizationException.php' );
require_once( 'Facebook/GraphObject.php' );
require_once( 'Facebook/GraphUser.php' );
require_once( 'Facebook/GraphSessionInfo.php' );
require_once( 'Facebook/Entities/AccessToken.php');
require_once( 'Facebook/HttpClients/FacebookCurl.php' );
require_once( 'Facebook/HttpClients/FacebookHttpable.php');
require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php');

// Use namespaces

	use Facebook\FacebookSession;
	use Facebook\FacebookRedirectLoginHelper;
	use Facebook\FacebookRequest;
	use Facebook\FacebookResponse;
	use Facebook\FacebookSDKException;
	use Facebook\FacebookRequestException;
	use Facebook\FacebookAuthorizationException;
	use Facebook\GraphObject;
	use Facebook\GraphUser;
	use Facebook\GraphSessionInfo;
	use Facebook\FacebookHttpable;
	use Facebook\FacebookCurlHttpClient;
	use Facebook\FacebookCurl;
    
	//  include config data
    require_once('config.php');	
  
    
FacebookSession::setDefaultApplication($app_id, $app_secret);

// getting fb session
$session  = new FacebookSession( $_SESSION['token'] );

// checking if status is posted or not
if (isset($_POST["status_update"]))
 {
    $status = htmlspecialchars($_POST["message"]); 
    $response = (new FacebookRequest(
    $session, 'POST', '/me/feed', array(
      'message' => $status
    )
  ))->execute()->getGraphObject()->asArray();
  
  // if successfully posted
  if (isset($response))
   {
     echo "<script>
                 alert('Status Posted.');     
          </script>";
   }

}
?>

<!DOCTYPE HTML>
<html> 
 <head>
   <meta name="viewport" content="width=device-width, initial-scale=1"/> 
   <link href='http://insignia14.in/style.css' rel="stylesheet" type="text/css"/> 
   <title>Post Status</title>
 </head>
 <body>
    <div class="header">
        <a href='<?php echo $_SESSION["log_out"]; ?>' id='cancel_button' style="text-decoration:none;">Log Out</a>    
    	<h4 id="heading">PostUpdate v1.0</h4>
    </div>
    <div align="center">
        <form action=""  method="post">
            <textarea id='text_box' placeholder="What's on your mind?" name="message"></textarea><br />
            <input type="submit" id="login_button" value="Post" name="status_update"/>
        </form>  
    </div>
 </body>
</html>
