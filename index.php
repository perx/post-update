<?php
ob_start();

// Start Session
session_start();
?>

<!DOCTYPE HTML>
<html>	
 <head>
 <meta name="viewport" content="width=device-width, initial-scale=1"/>
 <link href='http://insignia14.in/style.css' rel="stylesheet" type="text/css"/>
 </head>
 <body>
    <div class="header">
    	<button id="cancel_button" onclick="window.location='https://fb.com'" type="button">cancel</button>
    	<h4 id="heading">Login</h4>
    </div>
    <div align="center">
    	<p>
    		To use this application, please sign in with your Facebook account.
    	</p>
    </div>
 <div align="center">
<?php

// Including SDK 
require_once( 'Facebook/FacebookSession.php');
require_once( 'Facebook/FacebookRequest.php' );
require_once( 'Facebook/FacebookResponse.php' );
require_once( 'Facebook/FacebookSDKException.php' );
require_once( 'Facebook/FacebookRequestException.php' );
require_once( 'Facebook/FacebookRedirectLoginHelper.php');
require_once( 'Facebook/FacebookAuthorizationException.php' );
require_once( 'Facebook/GraphObject.php' );
require_once( 'Facebook/GraphUser.php' );
require_once( 'Facebook/GraphSessionInfo.php' );
require_once( 'Facebook/Entities/AccessToken.php');
require_once( 'Facebook/HttpClients/FacebookCurl.php' );
require_once( 'Facebook/HttpClients/FacebookHttpable.php');
require_once( 'Facebook/HttpClients/FacebookCurlHttpClient.php');


// Use namespaces

	use Facebook\FacebookSession;
	use Facebook\FacebookRedirectLoginHelper;
	use Facebook\FacebookRequest;
	use Facebook\FacebookResponse;
	use Facebook\FacebookSDKException;
	use Facebook\FacebookRequestException;
	use Facebook\FacebookAuthorizationException;
	use Facebook\GraphObject;
	use Facebook\GraphUser;
	use Facebook\GraphSessionInfo;
	use Facebook\FacebookHttpable;
	use Facebook\FacebookCurlHttpClient;
	use Facebook\FacebookCurl;


	// include config data
    require_once('config.php');	
     
	 // Initialize application, create helper object and get fb sess
	 FacebookSession::setDefaultApplication($app_id, $app_secret);
	 $helper = new FacebookRedirectLoginHelper($redirect_url);
	 
         
	 try {
  		$session = $helper->getSessionFromRedirect();
		} catch(FacebookRequestException $ex) {
  		// When Facebook returns an error
         echo "Exception occured, code". $ex->getCode();
		} catch(\Exception $ex) {
  		// When validation fails or other local issues\
         echo "Exception occured, code". $ex->getCode();
		}
        
	    // if fb sess exists  
	 	if($session){
 		// create request object,execute and capture response
		$request = new FacebookRequest($session, 'GET', '/me');
		// from response get graph object
		$response = $request->execute();
		$graph = $response->getGraphObject();

                
        // pass value to next page using sessions
        $_SESSION["token"] = $session -> getToken();
        $_SESSION["session"] = $session;
        $_SESSION["log_out"] = $helper -> getLogoutUrl($session, 'http://insignia14.in/logout.php');
       
        // redirect to post status page
        header("Location:http://insignia14.in/post.php");
        exit();
	}
	else{
		// else echo login
		echo '<a href="' . $helper->getLoginUrl( array( 'publish_actions' ) ) . '" id="login_button">Login with Facebook</a>';
	}

?>

</div>    
 </body>
</html>
